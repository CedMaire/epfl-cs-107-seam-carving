import java.util.ArrayList;

/**
 * @author Benjamin Deleze and Cedric Maire
 */
public final class Seam {

    /**
     * Compute shortest path between {@code from} and {@code to}
     * @param successors adjacency list for all vertices
     * @param costs weight for all vertices
     * @param from first vertex
     * @param to last vertex
     * @return a sequence of vertices, or {@code null} if no path exists
     */
    public static int[] path(int[][] successors, float[] costs, int from, int to) {
        // TODO path
        
    	// Constants needed
    	final int SUCCESSORS_ROWS = successors.length;		//Get the number of rows of successors
    	
    	// Creates two new arrays, one will simulate the distance and one will store the best predecessor
    	float[] distance = new float[SUCCESSORS_ROWS];
    	int[] bestPredecessor = new int[SUCCESSORS_ROWS];
		
    	// Initialize the distance to infinity for each slot
    	for (int i = 0; i < SUCCESSORS_ROWS; i++) {
    		
    		distance[i] = Float.POSITIVE_INFINITY;
    		
    		// Convention to tell if a slot is initialized or not
    		bestPredecessor[i] = -1;
    	}
    	
    	distance[from] = costs[from];
    	
    	// Find the best predecessor
    	boolean modified = true;
    	while (modified) {
    		modified = false;
    		
    		for (int i = 0; i < SUCCESSORS_ROWS; i++) {
    			for (int j = 0; j < successors[i].length; j++) {
    				if ( distance[ successors[i][j] ] > distance[i] + costs[ successors[i][j] ] ) {
    					
    					distance[ successors[i][j] ] = distance[i] + costs[ successors[i][j] ];
    					bestPredecessor[ successors[i][j] ] = i;
    					
    					modified = true;
    				}
    			}
    		}
    	}
    	
    	// Creates a new array list to store the path in reverse
    	ArrayList<Integer> pathReverse = new ArrayList<Integer>();
    	
    	// Fills the new created array list with the path
    	while (to != from && bestPredecessor[to] != -1) {
    		
    		pathReverse.add(to);
    		to = bestPredecessor[to];
    	}
    	
    	// Adds the last one (or the first, if you consider the right order)
    	pathReverse.add(from);
    	
    	// Checks if it was a success and then creates a new array to store the best path
    	if (to == from) {
    		
    		final int PATH_REVERSE_SIZE = pathReverse.size();	//Get the size of path_reverse
    		
    		int[] bestPath = new int[PATH_REVERSE_SIZE];
    		
    		final int BEST_PATH_ROWS = bestPath.length;			//Get the number of rows of bestPath
        	
        	for (int j = 0; j < BEST_PATH_ROWS; j++) {
        		
        		bestPath[j] = pathReverse.get(BEST_PATH_ROWS - j - 1);
        	}
        	
        	return bestPath;
    	}
    	
    	// If it was not a success it return null
    	else {
    		
    		return null;
    	}
    }

    /**
     * Find best seam
     * @param energy weight for all pixels
     * @return a sequence of x-coordinates (the y-coordinate is the index)
     */
    public static int[] find(float[][] energy) {
        // TODO find
        
    	// Constants needed
    	final int ENERGY_ROWS = energy.length;									//Get the number of rows of energy
    	final int ENERGY_COLUMNS = energy[0].length;							//Get the number of columns of energy
    	final int ENERGY_ROWS_TIMES_COLUMNS = ENERGY_ROWS * ENERGY_COLUMNS;		//Get the product from row and columns of energy
    	
    	// Creates two new arrays to store the pixels and their cost
    	int[][] successors = new int[ENERGY_ROWS_TIMES_COLUMNS + 2][];
    	float[] costs = new float[ENERGY_ROWS_TIMES_COLUMNS + 2];
    	
    	// The first pixel is connected two the whole first row
    	successors[ENERGY_ROWS_TIMES_COLUMNS] = new int[ENERGY_COLUMNS];
    	for (int i = 0; i < ENERGY_COLUMNS; i++) {
    		
    		successors[ENERGY_ROWS_TIMES_COLUMNS][i] = i;
    	}
    	
    	// The cost of this pixel is equal to 0
    	costs[ENERGY_ROWS_TIMES_COLUMNS] = 0;
    	
    	// The final pixel has no successor
    	successors[ENERGY_ROWS_TIMES_COLUMNS + 1] = new int[] {};
		costs[ENERGY_ROWS_TIMES_COLUMNS + 1] = 0;
    	
		// Initialize the successors and costs of the pixels of the last row
		for (int i = 0; i < energy[ENERGY_ROWS - 1].length; i++) {
			
			successors[ (ENERGY_ROWS - 1) * ENERGY_COLUMNS + i ] = new int[] {ENERGY_ROWS * ENERGY_COLUMNS + 1};
			costs[ (ENERGY_ROWS - 1) * ENERGY_COLUMNS + i ] = energy[ENERGY_ROWS - 1][i];
		}
		
		// And now for all the other pixels
    	for (int i = 0; i < ENERGY_ROWS - 1; i++) {
    		for (int j = 0; j < ENERGY_COLUMNS; j++) {
    			
    			final int ROW = i + 1;			//Get the value of the row
    			final int COLUMN_1 = j - 1;		//Get the value of the previous column
    			final int COLUMN_2 = j;			//Get the value of the column
    			final int COLUMN_3 = j + 1;		//Get the value of the next column
    			
    			if (COLUMN_1 < 0) {
    				
    				successors[i * ENERGY_COLUMNS + j] = new int[] {ROW * ENERGY_COLUMNS + COLUMN_2, ROW * ENERGY_COLUMNS + COLUMN_3};
    			}
    			
    			else if (COLUMN_3 == ENERGY_COLUMNS) {
    				
    				successors[i * ENERGY_COLUMNS + j] = new int[] {ROW * ENERGY_COLUMNS + COLUMN_1, ROW * ENERGY_COLUMNS + COLUMN_2};
    			}
    			
    			else {
    				
    				successors[i * ENERGY_COLUMNS + j] = new int[] {ROW * ENERGY_COLUMNS + COLUMN_1, ROW * ENERGY_COLUMNS + COLUMN_2, ROW * ENERGY_COLUMNS + COLUMN_3};
    			}
    			
    			costs[i * ENERGY_COLUMNS + j] = energy[i][j];
    		}
    	}
    	
    	// Creates a new array to call the best path
    	int[] indices = path(successors, costs, ENERGY_ROWS * ENERGY_COLUMNS, ENERGY_ROWS * ENERGY_COLUMNS + 1);
    	
    	// Creates a new array to store the final result
    	int[] result = new int[ENERGY_ROWS];
    	
    	for (int i = 0; i < ENERGY_ROWS; i++) {
    		
    		result[i] = indices[i + 1] % ENERGY_COLUMNS;
    	}
    	
        return result;
    }

    /**
     * Draw a seam on an image
     * @param image original image
     * @param seam a seam on this image
     * @return a new image with the seam in blue
     */
    public static int[][] merge(int[][] image, int[] seam) {
    	
        // Copy image
        int width = image[0].length;
        int height = image.length;
        int[][] copy = new int[height][width];
        
        for (int row = 0; row < height; ++row)
            for (int col = 0; col < width; ++col)
                copy[row][col] = image[row][col];

        // Paint seam in blue
        for (int row = 0; row < height; ++row)
            copy[row][seam[row]] = 0x0000ff;

        return copy;
    }

    /**
     * Remove specified seam
     * @param image original image
     * @param seam a seam on this image
     * @return the new image (width is decreased by 1)
     */
    public static int[][] shrink(int[][] image, int[] seam) {
    	
    	int height = image.length;
        int width = image[0].length;
        
        int[][] result = new int[height][width - 1];
        
        for (int row = 0; row < height; ++row) {
            for (int col = 0; col < seam[row]; ++col)
                result[row][col] = image[row][col];
            for (int col = seam[row] + 1; col < width; ++col)
                result[row][col - 1] = image[row][col];
        }
        
        return result;
    }

}
