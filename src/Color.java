
/**
 * @author Benjamin Deleze and Cedric Maire
 */
public final class Color {

    /**
     * Returns red component from given packed color.
     * @param rgb 32-bits RGB color
     * @return a float between 0.0 and 1.0
     * @see #getGreen
     * @see #getBlue
     * @see #getRGB(float, float, float)
     */
    public static float getRed(int rgb) {
        // TODO getRed
        
    	// Eliminate what is on the right side from the color red
    	int red = rgb >> 16;
    	
    	// Eliminate what is on the left side from the color red
    	red = red & 0b11111111;
    	
    	float redFloat = red / 255.0f;
        return redFloat;
    }

    /**
     * Returns green component from given packed color.
     * @param rgb 32-bits RGB color
     * @return a float between 0.0 and 1.0
     * @see #getRed
     * @see #getBlue
     * @see #getRGB(float, float, float)
     */
    public static float getGreen(int rgb) {
        // TODO getGreen
        
    	// Eliminate what is on the right side from the color green
    	int green = rgb >> 8;
    	
    	// Eliminate what is on the left side from the color green
    	green = green & 0b11111111;
    	
    	float greenFloat = green / 255.0f;
        return greenFloat;
    }

    /**
     * Returns blue component from given packed color.
     * @param rgb 32-bits RGB color
     * @return a float between 0.0 and 1.0
     * @see #getRed
     * @see #getGreen
     * @see #getRGB(float, float, float)
     */
    public static float getBlue(int rgb) {
        // TODO getBlue
        
    	// Eliminate what is on the left side from the color blue
    	int blue = rgb & 0b11111111;
    	
    	float blueFloat = blue / 255.0f;
        return blueFloat;
    }
    
    /**
     * Returns the average of red, green and blue components from given packed color.
     * @param rgb 32-bits RGB color
     * @return a float between 0.0 and 1.0
     * @see #getRed
     * @see #getGreen
     * @see #getBlue
     * @see #getRGB(float)
     */
    public static float getGray(int rgb) {
        // TODO getGray
        
    	// Compute the average of the three colors (red, green, blue)
    	return ( getRed(rgb) + getGreen(rgb) + getBlue(rgb) ) / 3;
    }

    /**
     * Returns packed RGB components from given red, green and blue components.
     * @param red a float between 0.0 and 1.0
     * @param green a float between 0.0 and 1.0
     * @param blue a float between 0.0 and 1.0
     * @return 32-bits RGB color
     * @see #getRed
     * @see #getGreen
     * @see #getBlue
     */
    public static int getRGB(float red, float green, float blue) {
        // TODO getRGB
        
    	// This checks the limits of the colors (they should be a float between 0 and 1)
    	red = checkColor(red);
    	green = checkColor(green);
    	blue = checkColor(blue);
    	
    	// Conversion in binary
    	final int RED_BINARY = (int) (red * 255);			//Get the color red in binary 
    	final int GREEN_BINARY = (int) (green * 255);		//Get the color green in binary
    	final int BLUE_BINARY = (int) (blue * 255);			//Get the color blue in binary
    	
    	// Concatenation of the colors (float) in a binary number
    	int rgbBinary = RED_BINARY << 8;
    	rgbBinary += GREEN_BINARY;
    	rgbBinary = rgbBinary << 8;
    	rgbBinary += BLUE_BINARY;
    	
    	return rgbBinary;
    }
    
    // This checks the limits of the colors (they should be a float between 0 and 1)
    public static float checkColor(float color) {

    	if (color > 1) {
    		
    		color = 1;
    	}
    	else if (color < 0) {
    		
    		color = 0;
    	}
    	
    	return color;
    }
    
    /**
     * Returns packed RGB components from given grayscale value.
	 * @param gray a float between 0.0 and 1.0
     * @return 32-bits RGB color
     * @see #getGray
     */
    public static int getRGB(float gray) {
        // TODO getRGB
        
    	// This checks the limits of the color gray
    	gray = checkColor(gray);
    	
    	// Conversion in binary
    	final int GRAY_BINARY = (int) (gray * 255);		//Get the gray value in binary
    	
    	// Concatenation of the color gray in a binary number
    	int grayBinary = GRAY_BINARY << 8;
    	grayBinary += GRAY_BINARY << 0;
    	grayBinary = grayBinary << 8;
    	grayBinary += GRAY_BINARY;
    	
        return grayBinary;
    }

    /**
     * Converts packed RGB image to grayscale float image.
     * @param image a HxW int array
     * @return a HxW float array
     * @see #toRGB
     * @see #getGray
     */
    public static float[][] toGray(int[][] image) {
        // TODO toGray
        
    	// Constants needed
    	final int IMAGE_ROWS = image.length;			//Get the number of rows of the image
    	final int IMAGE_COLUMNS = image[0].length;		//Get the number of columns of the image
    	
    	// Creates an array for the gray image
    	float[][] imageToGray = new float[IMAGE_ROWS][IMAGE_COLUMNS];
    	
    	// Fills the array with the gray values
    	for (int i = 0; i < IMAGE_ROWS; i++) {
    		for (int j = 0; j < IMAGE_COLUMNS; j++) {
    			
    			imageToGray[i][j] = getGray(image[i][j]);
    		}
    	}
    	
        return imageToGray;
    }

    /**
     * Converts grayscale float image to packed RGB image.
     * @param channels a HxW float array
     * @return a HxW int array
     * @see #toGray
     * @see #getRGB(float)
     */
    public static int[][] toRGB(float[][] gray) {
        // TODO toRGB
        
    	// Constants needed
    	final int GRAY_ROWS = gray.length;			//Get the number of rows of gray
    	final int GRAY_COLUMNS = gray[0].length;	//Get the number of columns of gray
    	
    	// Creates an array for the RGB image
    	int[][] imageToRGB = null;
    	
    	if (gray != null) {
    		
    		imageToRGB = new int[GRAY_ROWS][GRAY_COLUMNS];
    		
    		// Fills the array with the RGB values
    		for (int i = 0; i < GRAY_ROWS; i++) {
        		for (int j = 0; j < GRAY_COLUMNS; j++) {
        			
        			imageToRGB[i][j] = getRGB(gray[i][j]);
        		}
        	}
    	} 
    	
        return imageToRGB;
    }
}
