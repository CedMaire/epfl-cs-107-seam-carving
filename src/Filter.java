
/**
 * @author Benjamin Deleze and Cedric Maire
 */
public final class Filter {

    /**
     * Get a pixel without accessing out of bounds
     * @param gray a HxW float array
     * @param row Y coordinate
     * @param col X coordinate
     * @return nearest valid pixel color
     */
    public static float at(float[][] gray, int row, int col) {
        // TODO at
        
    	// The variable pixel we need is created
    	float pixel = 0.0f;
    	
    	// Constants needed
    	final int GRAY_ROWS = gray.length;			//Get the number of rows of gray
    	final int GRAY_COLUMNS = gray[0].length;	//Get the number of columns of gray
    	
    	if (row < 0) {
    		
    		// If the pixel is on top and to the left (but not in the image)
    		if (col < 0) {
    			
    			pixel = gray[0][0];
    		}
    		
    		// If the pixel is on top and to the right (but not in the image)
    		else if (col > GRAY_COLUMNS - 1) {
    			
    			pixel = gray[0][GRAY_COLUMNS - 1];
    		}
    		
    		// If the pixel is on top and in the center (but not in the image)
    		else {
    			
    			pixel = gray[0][col];
    		}
    	}
    	
    	else if (row > GRAY_ROWS - 1) {
    		
    		// If the pixel is on the bottom and to the right (but not in the image)
			if (col < 0) {
				
				pixel = gray[GRAY_ROWS - 1][0];
			}
			
			// If the pixel is on the bottom and to the left (but not in the image)
			else if (col > GRAY_COLUMNS - 1) {
				
				pixel = gray[GRAY_ROWS - 1][GRAY_COLUMNS - 1];
			}
			
			// If the pixel is on the bottom and in the center (but not in the image)
			else {
				
				pixel = gray[GRAY_ROWS - 1][col];
			}
    	}
    	
    	// If the pixel is on the left to the image (but not in the image)
    	else if (col < 0) {
    		
    		pixel = gray[row][0];
    	}
    	
    	// If the pixel is on the right to the image (but not in the image)
    	else if (col > GRAY_COLUMNS - 1) {
    		
    		pixel = gray[row][GRAY_COLUMNS - 1];
    	}
    	
    	// If the pixel is in the image
    	else {
    		
    		pixel = gray[row][col];
    	}
    	
    	return pixel;
    }

    /**
     * Convolve a single-channel image with specified kernel.
     * @param gray a HxW float array
     * @param kernel a MxN float array, with M and N odd
     * @return a HxW float array
     */
    public static float[][] filter(float[][] gray, float[][] kernel) {
        // TODO filter
        
    	// Constants needed
    	final int GRAY_ROWS = gray.length;				//Get the number of rows of gray
    	final int GRAY_COLUMNS = gray[0].length;		//Get the number of columns of gray
    	final int KERNEL_ROWS = kernel.length;			//Get the number of rows of the kernel
    	final int KERNEL_COLUMNS = kernel[0].length;	//Get the number of columns of the kernel
    	
    	// Creates a new array that will be the image after the kernel was applied
    	float[][] imageKernelized = new float[GRAY_ROWS][GRAY_COLUMNS];
    	
    	// Applies the kernel to the pixels
    	for (int i = 0; i < GRAY_ROWS; i++) {
    		for (int j = 0; j < GRAY_COLUMNS; j++) {
    			
    			float sum = 0.0f;
    			
    			// Helper to reposition in the rows
    			int m = i - ( (KERNEL_ROWS / 2) + 1 );
    			
    			// Go trough the kernel and sum up the elements of the kernel and the pixels around the choosen pixel
    			for (int k = 0; k < KERNEL_ROWS; k++) {
    				
    				m++;
    				
    				// Helper to reposition in the columns
    				int n = j - ( (KERNEL_COLUMNS / 2) + 1 );
    				
    				for (int l = 0; l < KERNEL_COLUMNS; l++) {
    					
    					n++;
    					sum += ( at(gray, m, n) * kernel[k][l] );
    				}
    			}
    			
    			imageKernelized[i][j] = sum;
    		}
    	}
    	
        return imageKernelized;
    }

    /**
     * Smooth a single-channel image
     * @param gray a HxW float array
     * @return a HxW float array
     */
    public static float[][] smooth(float[][] gray) {
        // TODO smooth
        
    	// Creates the smooth kernel
    	float[][] kernelSmooth =   {{0.1f, 0.1f, 0.1f}, 
									{0.1f, 0.2f, 0.1f}, 
									{0.1f, 0.1f, 0.1f}};
    	
        return filter(gray, kernelSmooth);
    }

    /**
     * Compute horizontal Sobel filter
     * @param gray a HxW float array
     * @return a HxW float array
     */
    public static float[][] sobelX(float[][] gray) {
        // TODO sobelX
        
    	// Creates the sobelX kernel
    	float[][] kernelSobelX =   {{-1.0f, 0.0f, 1.0f}, 
									{-2.0f, 0.0f, 2.0f}, 
									{-1.0f, 0.0f, 1.0f}};
    	
        return filter(gray, kernelSobelX);
    }

    /**
     * Compute vertical Sobel filter
     * @param gray a HxW float array
     * @return a HxW float array
     */
    public static float[][] sobelY(float[][] gray) {
        // TODO sobelY
        
    	// Creates the sobelY kernel
    	float[][] kernelSobelY =   {{-1.0f, -2.0f, -1.0f}, 
    								{0.0f, 0.0f, 0.0f}, 
    								{1.0f, 2.0f, 1.0f}};
    	
        return filter(gray, kernelSobelY);
    }

    /**
     * Compute the magnitude of combined Sobel filters
     * @param gray a HxW float array
     * @return a HxW float array
     */
    public static float[][] sobel(float[][] gray) {
        // TODO sobel
        
    	// Constants needed
    	final int GRAY_ROWS = gray.length;			//Get the number of rows of gray
    	final int GRAY_COLUMNS = gray[0].length;	//Get the number of columns of gray
    	
    	// Creates a new array to contain the image after applying sobelXY
    	float[][] imageSobel = new float[GRAY_ROWS][GRAY_COLUMNS];
    	
    	float[][] imageSobelX = sobelX(gray);
    	float[][] imageSobelY = sobelY(gray);
    	
    	// Fills the new array imageSobel with the new image after applying sobelXY
    	for (int i = 0; i < GRAY_ROWS; i++) {
    		for (int j = 0; j < GRAY_COLUMNS; j++) {
    			
    			imageSobel[i][j] = (float) Math.sqrt( (imageSobelX[i][j]) * (imageSobelX[i][j]) + (imageSobelY[i][j]) * (imageSobelY[i][j]) );
    		}
    	}
    	
        return imageSobel;
    }
}
